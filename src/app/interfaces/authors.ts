import { IAuthorChild } from "./author-child";

export interface IAuthors{
    id: number,
    nama: string,
    email: string,
    list_buku: IAuthorChild
}