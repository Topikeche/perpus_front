import { IBooksChild } from "./books-child";

export interface IBooks{
    id: number,
    judul: string,
    penulis: IBooksChild
}