import { Component, OnInit } from '@angular/core';
import { BooksService } from '../../services/books.service';
import { AuthorService } from '../../services/author.service';

@Component({
  selector: 'app-book-add',
  templateUrl: './book-add.component.html',
  styleUrls: ['./book-add.component.css']
})
export class BookAddComponent implements OnInit {

  public book = [];
  public authors = [];

  constructor(
    private _booksService: BooksService,
    private _authorService: AuthorService
  ) { }

  ngOnInit() {
    this.getAuthors();
  }

  getAuthors(){
    this._authorService.getAuthors()
        .subscribe(data => this.authors = data['data']);
  }

  save(){
    let buku = {
      judul: this.book['judul'],
      pengarang: this.book['pengarang']
    }

    this._booksService.addBook(buku).subscribe(
      data => {
          console.log(buku);
      })
  }

}
