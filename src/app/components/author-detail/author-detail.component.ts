import { Component, OnInit } from '@angular/core';
      
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { AuthorService }  from '../../services/author.service';
import { identifierModuleUrl } from '@angular/compiler';

@Component({
  selector: 'app-author-detail',
  templateUrl: './author-detail.component.html',
  styleUrls: ['./author-detail.component.css']
})
export class AuthorDetailComponent implements OnInit {

  private author = [];
  private authorsBooks = [];

  constructor(
    private route: ActivatedRoute,
    private authorService: AuthorService,
    private location: Location
  ) { }

  ngOnInit() {
    this.getAuthor();
  }

  getAuthor(): void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.authorService.getAuthor(id)
    .subscribe(data => {
      this.author = data['data'][0],
      this.authorsBooks = data['data'][0]['list_buku']
      console.log(this.authorsBooks)
    })
  }

}
