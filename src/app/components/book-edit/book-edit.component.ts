import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BooksService } from '../../services/books.service';
import { Location } from '@angular/common';
import { AuthorService } from '../../services/author.service';
import { OnDestroy } from "@angular/core";

@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.component.html',
  styleUrls: ['./book-edit.component.css']
})
export class BookEditComponent implements OnInit {

  public book = [];
  //public bookDataPenulis: string = "";
  public bookDataPenulis = [];
  public authors = [];

  constructor(
    private authorService: AuthorService,
    private route: ActivatedRoute,
    private booksService: BooksService,
    private location: Location
  ) { }

  ngOnInit() {
    this.getBook();
    this.getAuthors();
  }

  getAuthors(){
    this.authorService.getAuthors()
        .subscribe(data => {
          this.authors = data['data']
        });
  }

  getBook(): void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.booksService.getBook(id)
    .subscribe(datane => {
      this.book = datane['data'][0],
      this.bookDataPenulis = datane['data'][0]['penulis'],
      console.log(this.book)
    })
  }

  save(){
    let book = {
      judul: this.book['judul'],
      pengarang: +this.bookDataPenulis['id']
    }
    console.log(book);
    this.booksService.updateBook(book, +this.book['id'])
        .subscribe(data => {
          console.log(data)
          this.goBack()
        },
        error => {
          console.error("error in saving update")
        })
  }

  goBack(): void {
    this.location.back();
  }

}
