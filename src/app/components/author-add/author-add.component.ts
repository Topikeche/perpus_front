import { Component, OnInit, Input } from '@angular/core';
import { AuthorService } from '../../services/author.service';

@Component({
  selector: 'app-author-add',
  templateUrl: './author-add.component.html',
  styleUrls: ['./author-add.component.css']
})
export class AuthorAddComponent implements OnInit {

  author = {};

  constructor(private _authorService: AuthorService) { }

  ngOnInit() {
  }

  save(){
    console.log('hai...')
    let penulis = {
      nama: this.author['nama'],
      email: this.author['email'],
      alamat: this.author['alamat']
    };

    this._authorService.addAuthor(penulis).subscribe(
      data => {
        console.log(data)
      },
      error => {
        console.error("error in saving")
      }
    )
  }


}
