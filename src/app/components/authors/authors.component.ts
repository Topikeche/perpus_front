import { Component, OnInit } from '@angular/core';
import { AuthorService } from '../../services/author.service';
import { IAuthors } from '../../interfaces/authors';
import { Router } from '@angular/router';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {

  public authors = [];
  rmath: IAuthors[];

  constructor(
    private _authorService: AuthorService,
    private _router: Router
  ) { }

  ngOnInit() {
    this.getAuthorData();
  }

  getAuthorData(){
    this._authorService.getAuthors()
        .subscribe(data => this.authors = data['data']);
  }

  delete(rmvAuth: number){
    console.log(rmvAuth);
    this._authorService.deleteAuthor(rmvAuth).subscribe(data => {
      if(data['status']){
        this.getAuthorData();
      }
    });
    // this._router.navigateByUrl('/AuthorsComponent');
  }

}
