import { Component, OnInit } from '@angular/core';
import { BooksService } from '../../services/books.service';
import { AuthorService } from '../../services/author.service';
import { AuthMod } from '../../interfaces/unused';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  private sectionTitle = 'Book List';
  books = [];

  eternalBook: AuthMod[];

  constructor(
    private _booksService: BooksService
  ) { }

  ngOnInit() {
    this.getBuku();
  }

  getBuku(){
    this._booksService.getBooks()
        .subscribe(buku => {
          this.books = buku['data']
          this.eternalBook = buku['data'];
        });
  }
  
  delete(rmAuth: AuthMod): void {
    this.eternalBook = this.eternalBook.filter(h => h !== rmAuth);
    this._booksService.deleteBook(rmAuth).subscribe(data => {
      if(data['status']){
        this.getBuku();
      }
    });
  }

}
