import { Component, OnInit, Input } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { AuthorService }  from '../../services/author.service';
import { identifierModuleUrl } from '@angular/compiler';
import { IAuthors } from '../../interfaces/authors';

@Component({
  selector: 'app-author-edit',
  templateUrl: './author-edit.component.html',
  styleUrls: ['./author-edit.component.css']
})
export class AuthorEditComponent implements OnInit {

  //@Input() author: IAuthors;
  public author = [];

  constructor(
    private route: ActivatedRoute,
    private authorService: AuthorService,
    private location: Location
  ) { }

  ngOnInit() {
    this.getAuthor();
  }

  getAuthor(): void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.authorService.getAuthor(id)
    .subscribe(data => {
      this.author = data['data'][0]
      console.log(this.author)
    })
  }

  save(){
    let penulis = {
        nama: this.author['nama'],
        email: this.author['email'],
        alamat: this.author['alamat']
    }
    this.authorService.updateAuthor(penulis, +this.author['id'])
        .subscribe(data => {
          console.log(data)
          this.goBack()
        },
        error => {
          console.error("error in saving update")
        })
  }

  goBack(): void {
    this.location.back();
  }

}
