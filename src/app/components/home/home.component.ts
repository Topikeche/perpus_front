import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  // template: `
  // <input [(ngModel)] = "data" type="text">
  // {{data}}
  // `,
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public books = [];
  public data = "";
  public ds = ["siji", "loro", "telu"];
  public dsing = "siji";

  constructor(private _http: HttpClient) { }

  ngOnInit() {
  }

}
