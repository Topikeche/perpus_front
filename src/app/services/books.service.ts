import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IBooks } from '../interfaces/books';
import { AuthMod } from '../interfaces/unused';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  private _bookUrl = "http://localhost:1337/buku";

  constructor(private http: HttpClient) { }

  getBooks(): Observable<IBooks[]>{
    return this.http.get<IBooks[]>(this._bookUrl);
  }

  getBook(id: number): Observable<IBooks>{
    return this.http.get<IBooks>(this._bookUrl+`/${id}`);
  }

  deleteBook (rmAuth: AuthMod | number): Observable<AuthMod> {
    const id = typeof rmAuth === 'number' ? rmAuth : rmAuth.id;
    const url = `${this._bookUrl}/${id}`;

    return this.http.delete<AuthMod>(url, httpOptions);
    // return this.http.delete(this._bookUrl+`/${id}`, httpOptions);
  }

  addBook(book){
    let body = JSON.stringify(book);
    return this.http.post(this._bookUrl, body, httpOptions);
  }

  updateBook(bookNew, id: number){
    let body = JSON.stringify(bookNew);
    return this.http.put(this._bookUrl+`/${id}`, body, httpOptions);
  }

}
