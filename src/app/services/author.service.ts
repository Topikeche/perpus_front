import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IAuthors } from '../interfaces/authors';
import { Observable, of } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthorService {
  
  private _authorUrl = "http://localhost:1337/pengarang";

  constructor(private http: HttpClient) { }

  getAuthors(): Observable<IAuthors[]>{
    return this.http.get<IAuthors[]>(this._authorUrl);
  }

  getAuthor(id: number): Observable<IAuthors>{
    return this.http.get<IAuthors>(this._authorUrl+`/${id}`);
  }

  addAuthor(author){
    let body = JSON.stringify(author);
    return this.http.post(this._authorUrl, body, httpOptions);
  }

  updateAuthor(authorNew, id: number){
    let body = JSON.stringify(authorNew);
    return this.http.put(this._authorUrl+`/${id}`, body, httpOptions);
  }

  deleteAuthor(id: number){
    return this.http.delete(this._authorUrl+`/${id}`, httpOptions);
  }
  
}
