import { NgModule } from '@angular/core';
//import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { BooksComponent } from './components/books/books.component';
import { BookAddComponent } from './components/book-add/book-add.component';
import { BookEditComponent } from './components/book-edit/book-edit.component';
import { AuthorsComponent } from './components/authors/authors.component';
import { AuthorDetailComponent } from './components/author-detail/author-detail.component';
import { AuthorAddComponent } from './components/author-add/author-add.component';
import { AuthorEditComponent } from './components/author-edit/author-edit.component';
import { HomeComponent } from './components/home/home.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'books', component: BooksComponent },
  { path: 'books/book-add', component: BookAddComponent },
  { path: 'books/book-edit/:id', component: BookEditComponent },
  { path: 'authors', component: AuthorsComponent },
  { path: 'authors/author-add', component: AuthorAddComponent },
  { path: 'authors/author-detail/:id', component: AuthorDetailComponent },
  { path: 'authors/author-edit/:id', component: AuthorEditComponent },
  { path: 'home', component: HomeComponent }
];

@NgModule({
  // imports: [
  //   CommonModule
  // ],
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
  //declarations: []
})
export class AppRoutingModule { }
