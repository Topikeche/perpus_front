import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BooksComponent } from './components/books/books.component';
import { AppRoutingModule } from './/app-routing.module';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { BookAddComponent } from './components/book-add/book-add.component';
import { AuthorDetailComponent } from './components/author-detail/author-detail.component';
import { AuthorsComponent } from './components/authors/authors.component';
import { AuthorAddComponent } from './components/author-add/author-add.component';
import { BookEditComponent } from './components/book-edit/book-edit.component';
import { AuthorService } from './services/author.service';
import { HttpClientModule } from '@angular/common/http';
import { AuthorEditComponent } from './components/author-edit/author-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    BooksComponent,
    NavbarComponent,
    HomeComponent,
    BookAddComponent,
    AuthorDetailComponent,
    AuthorsComponent,
    AuthorAddComponent,
    BookEditComponent,
    AuthorEditComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [AuthorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
